from typing import Any, Dict
import datetime
from datetime import datetime as dt, timezone, tzinfo
import time
from pathlib import Path

from aiohttp import web, ClientSession, TCPConnector
from aiohttp.web import Application, json_response

from pytest import fixture

from target.api.test import AppResolver, AppServer  # , AppClient
from target._logger import get_logger

# from ..conftest import patch_datetime

# pytest_plugins = ['asyncio.pytest_plugin']
LOGGER = get_logger()

HOST: str = 'api.app.com'
TOKEN: str = 'ER34gsSGGS34XCBKd7u'


class AppApi:

    def __init__(self) -> None:
        self.me = {"name": "John Doe", "id": "12345678901234567"}
        self.my_friends = {
            "data": [
                {"name": "Bill Doe", "id": "233242342342"},
                {"name": "Mary Doe", "id": "2342342343222"},
                {"name": "Alex Smith", "id": "234234234344"},
            ],
            "paging": {
                "cursors": {
                    "before": "QVFIUjRtc2c5NEl0ajN",
                    "after": "QVFIUlpFQWM0TmVuaDRad0dt",
                },
                "next": (
                    f"https://{HOST}/v3.9/12345678901234567/"
                    "friends?access_token=EAACEdEose0cB"
                ),
            },
            "summary": {"total_count": 3},
        }

        self.locations = {
            "locations": {
                "1": {
                    "id": "1", "name": "Everywhere", "parent_id": None, "created_at": 1649839191, "updated_at": 1649839191},
                "2": {
                    "id": "2", "name": "Nowhere", "parent_id": "1", "created_at": 1649839217, "updated_at": 1649839217}}}

        self.users = [
            {"name": "Eddy", "id": "1", "created_at": 1649839191, "updated_at": 1649839191},
            {"name": "Sabrina", "id": "2", "created_at": 1649839191, "updated_at": 1649839191},
            {"name": "John Doe", "id": "3", "created_at": 1649839217, "updated_at": 1649839217},
            {"name": "Elisabeth Doe", "id": "4", "created_at": 1649839217, "updated_at": 1649839217},
            {"name": "Mary Doe", "id": "5", "created_at": 1649839217, "updated_at": 1649839217}]
        self.users_0 = {
            "users": {
                "1": {"name": "Eddy", "id": "1", "created_at": 1649839191, "updated_at": 1649839191},
                "2": {"name": "Sabrina", "id": "2", "created_at": 1649839191, "updated_at": 1649839191},
                "3": {"name": "John Doe", "id": "3", "created_at": 1649839217, "updated_at": 1649839217},
            },
            "total": 5,
            "limit": 3,
            "offset": 0
        }
        self.users_1 = {
            "users": {
                "4": {"name": "Elisabeth Doe", "id": "4", "created_at": 1649839217, "updated_at": 1649839217},
                "5": {"name": "Mary Doe", "id": "5", "created_at": 1649839217, "updated_at": 1649839217},
            },
            "total": 5,
            "limit": 3,
            "offset": 1
        }
        self.no_users = {
            "users": {
            },
            "total": 5,
            "limit": 3,
            "offset": 9
        }

        self.tags_1 = {"tags": [{"user_id": "1", "tag_id": "1", "name": "Glorious"}]}

        self.tags_2 = {"tags": [{"user_id": "2", "tag_id": "1", "name": "Glorious"}]}

        self.tags_3 = {"tags": [{"user_id": "3", "tag_id": "1", "name": "Glorious"}]}

        self.tags_4 = {"tags": [{"user_id": "4", "tag_id": "1", "name": "Glorious"}]}

        self.tags_5 = {"tags": [{"user_id": "5", "tag_id": "1", "name": "Glorious"}]}

        routes = web.RouteTableDef()

        @routes.get('/v3.9/me')
        async def on_me(request: web.Request) -> web.StreamResponse:
            return json_response(self.me)

        @routes.get('/v3.9/me/friends')
        async def on_my_friends(request: web.Request) -> web.StreamResponse:
            return json_response(self.my_friends)

        @routes.get('/v3.9/locations')
        async def get_locations(request: web.Request) -> web.StreamResponse:
            return json_response(self.locations)

        @routes.post('/v3.9/locations')
        async def post_locations(request: web.Request) -> web.StreamResponse:
            location = await request.json()
            location.pop('id', None)
            return json_response({'id': 3, 'created_at': 1628663978, 'updated_at': 1628663978} | location)

        @routes.put('/v3.9/locations/2')
        async def put_locations(request: web.Request) -> web.StreamResponse:
            location = await request.json()
            return json_response(self.locations['locations']['2'] | location | {'updated_at': 1628663978})

        @routes.get('/v3.9/users')
        async def get_users(request: web.Request) -> web.StreamResponse:
            offset = int(request.rel_url.query.get('offset', '0'))
            limit = int(request.rel_url.query.get('limit', '3'))
            return json_response({
                'users': {
                    u['id']: u
                    for u in self.users[offset:min(len(self.users) + 1, offset + limit)]
                } if offset < len(self.users) else {},
                'total': len(self.users),
                'limit': limit,
                'offset': offset})

        @routes.get('/v3.9/users/1/tags')
        async def get_tags_1(request: web.Request) -> web.StreamResponse:
            return json_response(self.tags_1)

        @routes.get('/v3.9/users/2/tags')
        async def get_tags_2(request: web.Request) -> web.StreamResponse:
            return json_response(self.tags_2)

        @routes.get('/v3.9/users/3/tags')
        async def get_tags_3(request: web.Request) -> web.StreamResponse:
            return json_response(self.tags_3)

        @routes.get('/v3.9/users/4/tags')
        async def get_tags_4(request: web.Request) -> web.StreamResponse:
            return json_response(self.tags_4)

        @routes.get('/v3.9/users/5/tags')
        async def get_tags_5(request: web.Request) -> web.StreamResponse:
            return json_response(self.tags_5)

        @routes.get('/v3.9/params')
        async def get_params(request: web.Request) -> web.StreamResponse:
            offset = request.rel_url.query.get('offset', '')
            limit = request.rel_url.query.get('limit', '')
            result = "offset: {}, limit: {}".format(offset, limit)
            return web.Response(text=str(result))

        @routes.get('/v3.9/request_status')
        async def get_request_status(request: web.Request) -> web.StreamResponse:
            status: int = int(request.rel_url.query.get('status', '200'))
            return web.Response(status=status, text='Too Many Requests -- The client has reached or exceeded a rate limit, or the server is overloaded.')

        self.app = Application()
        self.app.add_routes(routes)


@fixture
def patch_datetime(monkeypatch):

    class mydatetime(dt):
        @classmethod
        def now(cls, tz: tzinfo = None):
            # NOTE: timestamp dt.fromtimestamp(1628663978.321056, tz=timezone.utc)
            d: dt = dt.strptime('2022-04-29 07:39:38.321056+01:00', '%Y-%m-%d %H:%M:%S.%f%z')
            return d.astimezone(tz) if tz else d

        @classmethod
        def utcnow(cls):
            # NOTE: timestamp dt.fromtimestamp(1628663978.321056, tz=timezone.utc)
            # return dt.strptime('2022-04-29 07:39:38.321056+01:00', '%Y-%m-%d %H:%M:%S.%f%z')
            return cls.now(timezone.utc).replace(tzinfo=None)

    monkeypatch.setattr(datetime, 'datetime', mydatetime)

    monkeypatch.setattr(time, 'time', mydatetime.utcnow().timestamp)


@fixture
def host() -> str:
    return HOST


@fixture  # (scope='session')
def app_api() -> Application:
    return AppApi()


@fixture
async def client_session(host: str, app_api: AppApi, ssl_cert: Path, ssl_key: Path) -> ClientSession:
    async with AppServer(app_api.app, str(ssl_cert), str(ssl_key)) as server:
        async with ClientSession(connector=TCPConnector(resolver=AppResolver({host: server.port}), ssl=False)) as client:
            yield client


@fixture
def patch_session(monkeypatch: Any, client_session: ClientSession) -> None:

    monkeypatch.setattr(ClientSession, 'get', client_session.get)
    monkeypatch.setattr(ClientSession, 'post', client_session.post)


@fixture  # (name='config', scope='session')
def config(host: str) -> Dict[str, Any]:
    '''Use custom configuration set'''

    return {
        'start_date': '1970-01-01T00:00:00Z',
        'user_agent': 'Eddy (me@world.com)',
        'selected': ['locations', 'users', 'tags'],
        'rate_limit': 99,
        'rate_period': 1.0,
        'url': f'https://{host}/v3.9'
    }


@fixture
def temp_path(tmpdir_factory: Any) -> Path:

    return tmpdir_factory.mktemp('root_dir')


@fixture
def state():
    '''Use expected state'''

    return {
        'currently_syncing': None,
        'bookmarks': {
            'users': {'initial_full_table_complete': True},
            'tags': {'initial_full_table_complete': True},
            'locations': {'initial_full_table_complete': True}}}


@fixture
def ssl_cert(temp_path: Path) -> Path:

    content: str = '''
-----BEGIN CERTIFICATE-----
MIIDADCCAegCCQCgevpPMuTTLzANBgkqhkiG9w0BAQsFADBCMQswCQYDVQQGEwJV
QTEQMA4GA1UECAwHVWtyYWluZTEhMB8GA1UECgwYSW50ZXJuZXQgV2lkZ2l0cyBQ
dHkgTHRkMB4XDTE2MDgwNzIzMTMwOFoXDTI2MDgwNTIzMTMwOFowQjELMAkGA1UE
BhMCVUExEDAOBgNVBAgMB1VrcmFpbmUxITAfBgNVBAoMGEludGVybmV0IFdpZGdp
dHMgUHR5IEx0ZDCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAOUgkn3j
X/sdg6GGueGDHCM+snIUVY3fM6D4jXjyBhnT3TqKG1lJwCGYR11AD+2SJYppU+w4
QaF6YZwMeZBKy+mVQ9+CrVYyKQE7j9H8XgNEHV9BQzoragT8lia8eC5aOQzUeX8A
xCSSbsnyT/X+S1IKdd0txLOeZOD6pWwJoc3dpDELglk2b1tzhyN2GjQv3aRHj55P
x7127MeZyRXwODFpXrpbnwih4OqkA4EYtmqFbZttGEzMhd4Y5mkbyuRbGM+IE99o
QJMvnIkjAfUo0aKnDrcAIkWCkwLIci9TIG6u3R1P2Tn+HYVntzQZ4BnxanbFNQ5S
9ARd3529EmO3BzUCAwEAATANBgkqhkiG9w0BAQsFAAOCAQEAXyiw1+YUnTEDI3C/
vq1Vn9pnwZALVQPiPlTqEGkl/nbq0suMmeZZG7pwrOJp3wr+sGwRAv9sPTro6srf
Vj12wTo4LrTRKEDuS+AUJl0Mut7cPGIUKo+MGeZmmnDjMqcjljN3AO47ef4eWYo5
XGe4r4NDABEk5auOD/vQW5IiIMdmWsaMJ+0mZNpAV2NhAD/6ia28VvSL/yuaNqDW
TYTUYHWLH08H6M6qrQ7FdoIDyYR5siqBukQzeqlnuq45bQ3ViYttNIkzZN4jbWJV
/MFYLuJQ/fNoalDIC+ec0EIa9NbrfpoocJ8h6HlmWOqkES4QpBSOrkVid64Cdy3P
JgiEWg==
-----END CERTIFICATE-----
'''

    file_path = temp_path.join('server.crt')
    with file_path.open(mode='w+t') as temp_file:
        temp_file.seek(0)
        temp_file.write(content)
        temp_file.flush()
    return file_path


@fixture
def ssl_key(temp_path: Path) -> Path:

    content: str = '''
-----BEGIN RSA PRIVATE KEY-----
MIIEowIBAAKCAQEA5SCSfeNf+x2DoYa54YMcIz6ychRVjd8zoPiNePIGGdPdOoob
WUnAIZhHXUAP7ZIlimlT7DhBoXphnAx5kErL6ZVD34KtVjIpATuP0fxeA0QdX0FD
OitqBPyWJrx4Llo5DNR5fwDEJJJuyfJP9f5LUgp13S3Es55k4PqlbAmhzd2kMQuC
WTZvW3OHI3YaNC/dpEePnk/HvXbsx5nJFfA4MWleulufCKHg6qQDgRi2aoVtm20Y
TMyF3hjmaRvK5FsYz4gT32hAky+ciSMB9SjRoqcOtwAiRYKTAshyL1Mgbq7dHU/Z
Of4dhWe3NBngGfFqdsU1DlL0BF3fnb0SY7cHNQIDAQABAoIBAG9BJ6B03VADfrzZ
vDwh+3Gpqd/2u6wNqvYIejk123yDATLBiJIMW3x0goJm7tT+V7gjeJqEnmmYEPlC
nWxQxT6AOdq3iw8FgB+XGjhuAAA5/MEZ4VjHZ81QEGBytzBaosT2DqB6cMMJTz5D
qEvb1Brb9WsWJCLLUFRloBkbfDOG9lMvt34ixYTTmqjsVj5WByD5BhzKH51OJ72L
00IYpvrsEOtSev1hNV4199CHPYE90T/YQVooRBiHtTcfN+/KNVJu6Rf/zcaJ3WMS
1l3MBI8HwMimjKKkbddpoMHyFMtSNmS9Yq+4a9w7XZo1F5rt88hYSCtAF8HRAarX
0VBCJmkCgYEA9HenBBnmfDoN857femzoTHdWQQrZQ4YPAKHvKPlcgudizE5tQbs0
iTpwm+IsecgJS2Rio7zY+P7A5nKFz3N5c0IX3smYo0J2PoakkLAm25KMxFZYBuz4
MFWVdfByAU7d28BdNfyOVbA2kU2eal9lJ0yPLpMLbH8+bbvw5uBS808CgYEA7++p
ftwib3DvKWMpl6G5eA1C2xprdbE0jm2fSr3LYp/vZ4QN2V6kK2YIlyUqQvhYCnxX
oIP3v2MWDRHKKwJtBWR4+t23PaDaSXS2Ifm0qhRxwSm/oqpAJQXbR7VzxXp4/4FP
1SgkLe51bubc4h+cDngqBLcplCanvj52CqhqzDsCgYAEIhG8zANNjl22BLWaiETV
Jh9bMifCMH4IcLRuaOjbfbX55kmKlvOobkiBGi3OUUd28teIFSVF8GiqfL0uaLFg
9XkZ1yaxe+or3HLjz1aY171xhFQwqcj4aDoCqHIE+6Rclr/8raxqXnRNuJY5DivT
okO5cdr7lpsjl83W2WwNmQKBgCPXi1xWChbXqgJmu8nY8NnMMVaFpdPY+t7j5U3G
+GDtP1gZU/BKwP9yqInblWqXqp82X+isjg/a/2pIZAj0vdB2Z9Qh1sOwCau7cZG1
uZVGpI+UavojsJ1XOKCHrJmtZ/HTIVfYPT9XRdehSRHGYwuOS8iUi/ODqr8ymXOS
IRINAoGBAMEmhTihgFz6Y8ezRK3QTubguehHZG1zIvtgVhOk+8hRUTSJPI9nBJPC
4gOZsPx4g2oLK6PiudPR79bhxRxPACCMnXkdwZ/8FaIdmvRHsWVs8T80wID0wthI
r5hW4uqi9CcKZrGWH7mx9cVJktspeGUczvKyzNMfCaojwzA/49Z1
-----END RSA PRIVATE KEY-----
'''

    file_path = temp_path.join('server.key')
    file_path.write_text(content, encoding='utf-8')
    return file_path


@fixture
def ssl_cert_req(temp_path: Path) -> Path:

    content: str = '''
-----BEGIN CERTIFICATE REQUEST-----
MIIChzCCAW8CAQAwQjELMAkGA1UEBhMCVUExEDAOBgNVBAgMB1VrcmFpbmUxITAf
BgNVBAoMGEludGVybmV0IFdpZGdpdHMgUHR5IEx0ZDCCASIwDQYJKoZIhvcNAQEB
BQADggEPADCCAQoCggEBAOUgkn3jX/sdg6GGueGDHCM+snIUVY3fM6D4jXjyBhnT
3TqKG1lJwCGYR11AD+2SJYppU+w4QaF6YZwMeZBKy+mVQ9+CrVYyKQE7j9H8XgNE
HV9BQzoragT8lia8eC5aOQzUeX8AxCSSbsnyT/X+S1IKdd0txLOeZOD6pWwJoc3d
pDELglk2b1tzhyN2GjQv3aRHj55Px7127MeZyRXwODFpXrpbnwih4OqkA4EYtmqF
bZttGEzMhd4Y5mkbyuRbGM+IE99oQJMvnIkjAfUo0aKnDrcAIkWCkwLIci9TIG6u
3R1P2Tn+HYVntzQZ4BnxanbFNQ5S9ARd3529EmO3BzUCAwEAAaAAMA0GCSqGSIb3
DQEBCwUAA4IBAQDO/PSd29KgisTdGXhntg7yBEhBAjsDW7uQCrdrPSZtFyN6wUHy
/1yrrWe56ZuW8jpuP5tG0eTZ+0bT2RXIRot8a2Cc3eBhpoe8M3d84yXjKAoHutGE
5IK+TViQdvT3pT3a7pTmjlf8Ojq9tx+U2ckiz8Ccnjd9yM47M9NgMhrS1aBpVZSt
gOD+zzrqMML4xks9id94H7bi9Tgs3AbEJIyDpBpoK6i4OvK7KTidCngCg80qmdTy
bcScLapoy1Ped2BKKuxWdOOlP+mDJatc/pcfBLE13AncQjJgMerS9M5RWCBjmRow
A+aB6fBEU8bOTrqCryfBeTiV6xzyDDcIXtc6
-----END CERTIFICATE REQUEST-----
'''

    file_path = temp_path.join('server.csr')
    file_path.write_text(content, encoding='utf-8')
    return file_path
