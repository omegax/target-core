'''Tests for the target.stream module'''
# Standard library imports
from copy import deepcopy
from datetime import datetime as dt, timezone
from decimal import getcontext
from pathlib import Path
import sys
import json
from typing import Callable

# Third party imports
from pytest import fixture, raises

# Package imports
from target.stream import (
    _add_metadata_columns_to_schema,
    _add_metadata_values_to_record,
    _remove_metadata_values_from_record,
    _all_precisions,
    emit_state,
    Loader,
    main
)

from .conftest import clear_dir


@fixture
def input_data():
    '''messages.json'''

    return Path('tests', 'resources', 'messages.json').read_text(encoding='utf-8')[:-1].split('\n')


@fixture
def invalid_row_data():
    '''invalid-json.json'''

    return [
        '{"type": "STATE", "value": {"currently_syncing": "tap_dummy_test-test_table_one"}}',
        '{"type": "SCHEMA", "stream": "tap_dummy_test-test_table_one", "schema": {"properties": {"c_pk": {"inclusion": "automatic", "minimum": -2147483648,'
        '"maximum": 2147483647, "type": ["null", "integer"]}, "c_varchar": {"inclusion": "available", "maxLength": 16, "type": ["null", "string"]}, "c_int":'
        '{"inclusion": "available", "minimum": -2147483648, "maximum": 2147483647, "type": ["null", "integer"]}}, "type": "object"},'
        '"key_properties": ["c_pk"]}',
        'THIS IS A TEST INPUT FROM A TAP WITH A LINE WITH INVALID JSON',
        '{"type": "ACTIVATE_VERSION", "stream": "tap_dummy_test-test_table_one", "version": 1}',
    ]


@fixture
def invalid_order_data():
    '''invalid-message-order.json'''

    return [
        '{"type": "SCHEMA", "stream": "tap_dummy_test-test_table_one", "schema": {"properties": {"c_pk": {"inclusion": "automatic", "minimum": -2147483648,'
        '"maximum": 2147483647, "type": ["null", "integer"]}, "c_varchar": {"inclusion": "available", "maxLength": 16, "type": ["null", "string"]},'
        '"c_int": {"inclusion": "available", "minimum": -2147483648, "maximum": 2147483647, "type": ["null", "integer"]}}, "type": "object"},'
        '"key_properties": ["c_pk"]}',
        '{"type": "RECORD", "stream": "tap_dummy_test-test_table_one", "record": {"c_pk": 1, "c_varchar": "1", "c_int": 1}, "version": 1,'
        '"time_extracted": "2019-01-31T15:51:47.465408Z"}',
        '{"type": "RECORD", "stream": "tap_dummy_test-test_table_two", "record": {"c_pk": 2, "c_varchar": "2", "c_int": 2, "c_date": "2019-02-10 02:00:00"},'
        '"version": 3, "time_extracted": "2019-01-31T15:51:48.861962Z"}',
    ]


def test_emit_state(capsys, state):
    '''TEST : simple emit_state call'''

    emit_state(state)
    captured = capsys.readouterr()
    assert captured.out == json.dumps(state) + '\n'

    emit_state(None)
    captured = capsys.readouterr()
    assert captured.out == ''


def test_add_metadata_columns_to_schema():
    '''TEST : simple _add_metadata_columns_to_schema call'''

    assert _add_metadata_columns_to_schema({
        "type": "SCHEMA",
        "stream": "tap_dummy_test-test_table_one",
        "schema": {
            "properties": {
                "c_pk": {
                    "inclusion": "automatic", "minimum": -2147483648, "maximum": 2147483647,
                    "type": ["null", "integer"]},
                "c_varchar": {
                    "inclusion": "available", "maxLength": 16, "type": ["null", "string"]},
                "c_int": {
                    "inclusion": "available", "minimum": -2147483648, "maximum": 2147483647,
                    "type": ["null", "integer"]}},
            "type": "object"},
        "key_properties": ["c_pk"]}) \
        == {
            'type': 'SCHEMA',
            'stream': 'tap_dummy_test-test_table_one',
            'schema': {
                'properties': {
                    'c_pk': {
                        'inclusion': 'automatic', 'minimum': -2147483648, 'maximum': 2147483647,
                        'type': ['null', 'integer']},
                    'c_varchar': {
                        'inclusion': 'available', 'maxLength': 16, 'type': ['null', 'string']},
                    'c_int': {
                        'inclusion': 'available', 'minimum': -2147483648, 'maximum': 2147483647,
                        'type': ['null', 'integer']},
                    '_sdc_batched_at': {
                        'type': ['null', 'string'], 'format': 'date-time'},
                    '_sdc_deleted_at': {'type': ['null', 'string']},
                    '_sdc_extracted_at': {'type': ['null', 'string'], 'format': 'date-time'},
                    '_sdc_primary_key': {'type': ['null', 'string']},
                    '_sdc_received_at': {'type': ['null', 'string'], 'format': 'date-time'},
                    '_sdc_sequence': {'type': ['integer']},
                    '_sdc_table_version': {'type': ['null', 'string']}},
                'type': 'object'},
            'key_properties': ['c_pk']}


def test_add_metadata_values_to_record():
    '''TEST : simple _add_metadata_values_to_record call'''

    assert _add_metadata_values_to_record({
        "type": "RECORD",
        "stream": "tap_dummy_test-test_table_one",
        "record": {
            "c_pk": 1, "c_varchar": "1", "c_int": 1, "c_float": 1.99},
        "version": 1, "time_extracted": "2019-01-31T15:51:47.465408Z"}, {}, dt.fromtimestamp(1628713605.321056, tz=timezone.utc)) \
        == {
            'c_pk': 1, 'c_varchar': '1', 'c_int': 1, 'c_float': 1.99,
            '_sdc_batched_at': '2021-08-11T20:26:45.321056',
            '_sdc_deleted_at': None,
            '_sdc_extracted_at': '2019-01-31T15:51:47.465408Z',
            '_sdc_primary_key': None,
            '_sdc_received_at': '2021-08-11T20:26:45.321056',
            '_sdc_sequence': 1628713605321,
            '_sdc_table_version': 1}


def test_remove_metadata_values_from_record():
    '''TEST : simple _remove_metadata_values_from_record call'''

    assert _remove_metadata_values_from_record({
        "type": "RECORD",
        "stream": "tap_dummy_test-test_table_one",
        "record": {
            "c_pk": 1, "c_varchar": "1", "c_int": 1, "c_float": 1.99,
            '_sdc_batched_at': '2021-08-11T21:16:22.420939',
            '_sdc_deleted_at': None,
            '_sdc_extracted_at': '2019-01-31T15:51:47.465408Z',
            '_sdc_primary_key': None,
            '_sdc_received_at': '2021-08-11T21:16:22.420939',
            '_sdc_sequence': 1628712982421,
            '_sdc_table_version': 1},
        "version": 1, "time_extracted": "2019-01-31T15:51:47.465408Z"}) \
        == {'c_pk': 1, 'c_varchar': '1', 'c_int': 1, 'c_float': 1.99}


# def test_float_to_decimal():
#     '''TEST : simple _float_to_decimal call'''

#     assert _float_to_decimal({
#         "type": "RECORD",
#         "stream": "tap_dummy_test-test_table_one",
#         "record": {
#             "c_pk": 1, "c_varchar": "1", "c_int": 1, "c_float": 1.99},
#         "version": 1, "time_extracted": "2019-01-31T15:51:47.465408Z"}) \
#         == {
#             "type": "RECORD", "stream": "tap_dummy_test-test_table_one",
#             "record": {
#                 "c_pk": 1, "c_varchar": "1", "c_int": 1, "c_float": Decimal('1.99')},
#             "version": 1, "time_extracted": "2019-01-31T15:51:47.465408Z"}


def test_all_precisions():
    '''TEST : simple _all_precisions call'''

    assert max(_all_precisions([]), default=getcontext().prec) == getcontext().prec

    get_schema: Callable = lambda go_deep: {
        "type": "SCHEMA",
        "stream": "tap_dummy_test-test_table_one",
        "schema": {
            "properties": {
                "c_pk": {
                    "type": "integer", "inclusion": "automatic", "minimum": -2147483648, "maximum": 2147483647
                },
                "value": {
                    "type": "number", "minimum": -1e+33, "maximum": 1e+33, "multipleOf": 1e-05, "exclusiveMinimum": True, "exclusiveMaximum": True
                },
                "values": [{
                    "type": "number", "minimum": -1e+33
                }],
                "internal_ids": {
                    "type": ["null", "object"],
                    "properties": {
                        "ref_a_id": {"type": ["null", "integer"], "multipleOf": 1e-05},
                        "extra": {
                            "type": ["null", "object"],
                            "properties": {
                                "something": {"inclusion": "available", "maxLength": 16, "type": ["null", "string"]},
                                "else": {"type": "number", "maximum": go_deep}
                            },
                            "additionalProperties": False
                        }
                    },
                    "additionalProperties": False
                },
                "external_ids": {
                    "type": ["null", "array"], "items": {"type": ["null", "string"]}
                },
                "filters": {
                    "type": ["null", "object"],
                    "properties": {
                        "ref_b_id": {"type": ["null", "number"], "minimum": -1e+33, "maximum": 1e+33},
                    },
                    "additionalProperties": False
                },
                "deleted": {"type": "boolean"}
            },
            "type": "object"},
        "key_properties": ["c_pk"]}

    assert max(_all_precisions(get_schema(1e+99))) == 99

    assert max(_all_precisions(get_schema(1e+198))) == 199


async def test_writelines(caplog, config, input_data, input_multi_stream_data, invalid_row_data, invalid_order_data, state, file_metadata):
    '''TEST : simple Loader writelines call'''

    loader: Loader = Loader(config)
    await loader.writelines(input_multi_stream_data)
    output_state, output_file_metadata = loader.state, loader.stream_data
    file_paths = set(path for path in Path(config['work_dir']).iterdir())

    assert output_state == state

    assert len(file_paths) == 3

    assert len(set(str(values['path'][1]['absolute_path']) for values in output_file_metadata.values()) - set(str(path) for path in file_paths)) == 0

    assert output_file_metadata['tap_dummy_test-test_table_three']['path'][1]['absolute_path'].read_text(encoding='utf-8')[:-1].split('\n') \
        == [json.dumps(item) for item in file_metadata['tap_dummy_test-test_table_three']['file_data']]

    # with open(output_file_metadata['tap_dummy_test-test_table_three']['path'][1]['absolute_path'], 'r', encoding='utf-8') as input_file:
    #     assert [item for item in input_file] == [json.dumps(item) + '\n' for item in file_metadata['tap_dummy_test-test_table_three']['file_data']]

    for compression in {'gzip': '.gz', 'lzma': '.xz', 'none': ''}:
        clear_dir(Path(config['work_dir']))
        config_copy = deepcopy(config)
        config_copy['compression'] = compression
        output_state, output_file_metadata = await Loader(config_copy).writelines(input_multi_stream_data)
        file_paths = set(path for path in Path(config['work_dir']).iterdir())

        assert len(file_paths) == 3

        assert len(set(str(values['path'][1]['absolute_path']) for values in output_file_metadata.values()) - set(str(path) for path in file_paths)) == 0

    clear_dir(Path(config['work_dir']))

    config_copy = deepcopy(config)
    config_copy['add_metadata_columns'] = True
    config_copy['asynchronous'] = False
    output_state, output_file_metadata = await Loader(config_copy).writelines(input_multi_stream_data)

    assert output_state == state

    clear_dir(Path(config['work_dir']))

    config_copy = deepcopy(config)
    config_copy['memory_buffer'] = 9
    output_state, output_file_metadata = await Loader(config_copy).writelines(input_multi_stream_data)

    assert output_state == state

    clear_dir(Path(config['work_dir']))

    dummy_type = '{"type": "DUMMY", "value": {"currently_syncing": "tap_dummy_test-test_table_one"}}'
    output_state, output_file_metadata = await Loader(config_copy).writelines([dummy_type] + input_multi_stream_data)

    assert 'Unknown line type "{}" in line "{}"'.format(json.loads(dummy_type)['type'], dummy_type.replace('"', "'")) in caplog.text

    with raises(json.decoder.JSONDecodeError):
        output_state, output_file_metadata = await Loader(config_copy).writelines(invalid_row_data)

    with raises(Exception):
        output_state, output_file_metadata = await Loader(config_copy).writelines(invalid_order_data)

    record = {
        "type": "RECORD",
        "stream": "tap_dummy_test-test_table_one",
        "record": {"c_pk": 1, "c_varchar": "1", "c_int": 1},
        "version": 1,
        "time_extracted": "2019-01-31T15:51:47.465408Z"}

    with raises(Exception):
        dummy_input_multi_stream_data = deepcopy(input_multi_stream_data)
        dummy_record = deepcopy(record)
        dummy_record.pop('stream')
        dummy_input_multi_stream_data.insert(3, json.dumps(dummy_record))
        output_state, output_file_metadata = await Loader(config_copy).writelines(dummy_input_multi_stream_data)

    schema = {
        "type": "SCHEMA",
        "stream": "tap_dummy_test-test_table_one",
        "schema": {
            "properties": {
                "c_pk": {"inclusion": "automatic", "minimum": -2147483648, "maximum": 2147483647, "type": ["null", "integer"]},
                "c_varchar": {"inclusion": "available", "maxLength": 16, "type": ["null", "string"]},
                "c_int": {"inclusion": "available", "minimum": -2147483648, "maximum": 2147483647, "type": ["null", "integer"]}},
            "type": "object"},
        "key_properties": ["c_pk"]}

    with raises(Exception):
        dummy_input_multi_stream_data = deepcopy(input_multi_stream_data)
        dummy_schema = deepcopy(schema)
        dummy_schema.pop('stream')
        dummy_input_multi_stream_data.insert(1, json.dumps(dummy_schema))
        output_state, output_file_metadata = await Loader(config_copy).writelines(dummy_input_multi_stream_data)

    with raises(Exception):
        dummy_input_multi_stream_data = deepcopy(input_multi_stream_data)
        dummy_schema = deepcopy(schema)
        dummy_schema.pop('key_properties')
        dummy_input_multi_stream_data.insert(1, json.dumps(dummy_schema))
        output_state, output_file_metadata = await Loader(config_copy).writelines(dummy_input_multi_stream_data)

    # NOTE: 2 distant waves of the same stream
    dummy_input_data = deepcopy(input_data)
    for item in input_data[-4:-7:-1]:
        dummy_input_data.insert(5, item)
    output_state, output_file_metadata = await Loader(config_copy).writelines(dummy_input_data)

    assert output_state == json.loads(input_data[-1])['value']

    assert output_file_metadata['users']['path'][1]['absolute_path'].read_text(encoding='utf-8')[:-1].split('\n') \
        == [json.dumps(json.loads(item)['record']) for item in input_data[1:3]] * 2


def test_main(capsys, patch_datetime, patch_sys_stdin, patch_argument_parser, state, file_metadata):
    '''TEST : simple main call'''

    main(lines=sys.stdin)

    captured = capsys.readouterr()
    assert captured.out == json.dumps(state) + '\n'

    for file_info in file_metadata.values():
        assert file_info['path'][1]['absolute_path'].exists()
