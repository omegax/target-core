[metadata]
name = target-core
version = attr: target.__version__
description = Singer.io Target Core features
long_description = file: README.md
long_description_content_type = text/markdown
author = Eddy ∆
author_email = edrdelta@gmail.com
url = https://gitlab.com/singer-core/target-core
keywords = etl, singer, singer-core, tap, tap-core, target, target-core, target-s3-jsonl, target-s3-json
license = Apache License 2.0
classifiers =
    Development Status :: 2 - Pre-Alpha
    Operating System :: OS Independent
    License :: OSI Approved :: Apache Software License
    Programming Language :: Python :: 3 :: Only
    Programming Language :: Python :: 3.9
    Programming Language :: Python :: 3.10
    Programming Language :: Python :: 3.11
project_urls =
    Documentation = https://singer-core.gitlab.io/target-core
    Releases = https://gitlab.com/singer-core/target-core/-/releases
    Changelog = https://gitlab.com/singer-core/target-core/blob/main/CHANGELOG.md
    Issue Tracker = https://gitlab.com/singer-core/target-core/issues
    Bug Tracker = https://gitlab.com/singer-core/target-core/issues/service_desk

[options]
packages = find:
package_dir =
    = src
# py_modules = target
python_requires = >=3.9
# install_requires = file: requirements.txt
# install_requires =
#     jsonschema==4.9.1
#     aiohttp==3.8.1
#     backoff==2.1.2
#     boto3==1.24.52
#     # aiobotocore==2.3.1
#     # aiobotocore[awscli]
#     # s3fs==2022.3.0
#     # pysimdjson>=4.0.3
#     # fire==0.4.0 # automate args parsing
include_package_data = True
platforms = any

[options.entry_points]
console_scripts =
    target-core = target:main

[options.package_data]
target = logging.conf

[options.packages.find]
where = src
exclude =
    tests
    docs

[options.extras_require]
test =
    pytest-asyncio
    pytest-cov
    # coverage[toml]
    # pytest-xdist
lint = flake8
static = mypy
dist = build
deploy = twine
docs =
    sphinx
    sphinx-rtd-theme
    sphinx-automodapi
    numpydoc

[sdist]
formats = zip, gztar

[tool:pytest]
addopts = -v --cov=target --cov-fail-under 95 --cov-report xml --cov-report term --cov-report html:htmlcov --doctest-modules --junitxml=report.xml
testpaths = tests
asyncio_mode = auto

[coverage:run]
branch = True
omit =
    ./setup.py
    tests/.*
    docs/conf.py
    venv/*

[coverage:report]
show_missing = True
skip_covered = False
exclude_lines =
    if __name__ == .__main__.:

[flake8]
count = True
show-source = True
statistics = True
extend-exclude = venv
    build
ignore = C901
max-line-length = 160
max-complexity = 10
# per-file-ignores =
#     # line too long
#     tests/conftest.py: F401,

[build_sphinx]
builder = html
warning-is-error = true
# keep-going = true
project = 'Target Core'
version = attr: target.__version__
release = attr: target.__version__
source-dir = 'docs'

[tox:tox]
passenv = TOXENV TOX_* CI_* GITLAB_*
# requires = tox-pipenv
envlist = py{39,310}
# labels =
#     test = py{39,310,pi}
#     static = flake8, mypy
# envlist = .virtualenvs/target-s3-jsonl
isolated_build = True
# skipsdist = false
# parallel_show_output=True

# requires = tox-pip-extensions
# tox_pip_extensions_ext_venv_update = true

[testenv]
usedevelop = True
extras = test
commands = pytest {posargs}
    # publish: python setup.py sdist upload --sign -r pypi
    # publish: echo Publish that

[testenv:lint]
usedevelop = True
skip_install = true
deps = flake8
commands = flake8 {posargs}

[testenv:static]
usedevelop = True
skip_install = true
deps = mypy
commands = mypy {posargs}

[testenv:coverage]
usedevelop = True
passenv = CODECOV_TOKEN CI_*
skip_install = true
deps = codecov
# allowlist_externals = gpg
# install_command = echo Install codecov {packages}
#     curl https://keybase.io/codecovsecurity/pgp_keys.asc | gpg --no-default-keyring --keyring trustedkeys.gpg --import # One-time step
#     curl -Os https://uploader.codecov.io/latest/linux/codecov
#     curl -Os https://uploader.codecov.io/latest/linux/codecov.SHA256SUM
#     curl -Os https://uploader.codecov.io/latest/linux/codecov.SHA256SUM.sig
#     gpgv codecov.SHA256SUM.sig codecov.SHA256SUM
#     shasum -a 256 -c codecov.SHA256SUM
#     chmod +x ./codecov
commands = 
    codecov \
    --file "{toxinidir}/coverage.xml" \
    --name "codecov-$CI_PROJECT_NAME" \
    --branch "$CI_COMMIT_BRANCH" \
    --commit "$CI_COMMIT_SHA" \
    --tag "$CI_COMMIT_TAG" \
    --flags "unittests" {posargs} || echo 'Codecov upload failed'

[testenv:docs]
# https://packaging-guide.openastronomy.org/en/latest/docs.html
# Init
# sphinx-quickstart docs; cd docs
# edit index.rst >>> add modules
# sphinx-apidoc -o docs .
# sphinx-apidoc -o /source/_modules src
# sphinx-build docs docs/_build/html -W -j auto --color -b html
description = Invoke sphinx-build to build the HTML docs
usedevelop = True
extras = docs
# commands_pre = sphinx-build docs/source "{toxworkdir}/docs_out" -d "{toxworkdir}/docs_doctree" -b doctest {posargs:-E}
# commands = sphinx-build docs docs/_build/html -W -j auto --color -Ea -b html {posargs}
commands = sphinx-build docs/source "{toxworkdir}/docs_out" -d "{toxworkdir}/docs_doctree" -W -j auto --color -b html {posargs}
commands_post = python -c 'import pathlib; print("documentation available under file://\{0\}".format(pathlib.Path(r"{toxworkdir}") / "docs_out" / "index.html"))'
    # sphinx-build docs/source "{toxworkdir}/docs_out" -d "{toxworkdir}/docs_doctree" -b linkcheck {posargs:-E}

[testenv:dist]
deps = build
commands = python -m build

[testenv:deploy]
usedevelop = True
skip_install = true
# depends = dist
passenv = TWINE_*
deps =
    build
    twine
commands_pre =
    python -m build
    twine check dist/*
commands = twine upload --skip-existing {posargs} dist/*

# [bumpversion]
# current_version = 0.0.3
# commit = True
# tag = False
# parse = (?P<major>\d+)\.(?P<minor>\d+)\.(?P<patch>\d+)(\-(?P<release>[a-z]+)(?P<build>\d+))?
# serialize = 
# 	{major}.{minor}.{patch}-{release}{build}
# 	{major}.{minor}.{patch}

# [bumpversion:part:release]
# values = dev

# [bumpversion:file:src/target/__init__.py]
# search = __version__ = '{current_version}'
# replace = __version__ = '{new_version}'

# # [bumpversion:file:pyproject.toml]
# # search = version = "{current_version}"
# # replace = version = "{new_version}"

# [bumpversion:file:docs/source/conf.py]
# search = release = '{current_version}'
# replace = release = '{new_version}'

# # [bumpversion:file:cookiecutter/tap-template/{{cookiecutter.tap_id}}/pyproject.toml]
# # search = singer-sdk = "^{current_version}"
# # replace = singer-sdk = "^{new_version}"

# # [bumpversion:file:cookiecutter/target-template/{{cookiecutter.target_id}}/pyproject.toml]
# # search = singer-sdk = "^{current_version}"
# # replace = singer-sdk = "^{new_version}"
